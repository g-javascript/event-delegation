/**
 * Hay una lista de libros contenidos en el ul que tiene un id books
 * El event delegation dice
 *  que debemos agregar el event listeners al padre
 *  cuando se le de click a uno de los li, por medio del bubbling va a ir subiendo de a hasta ul, vamos a saber a que a se dió click, por medio del target, sin necesidad de agregar event listeners específicos para cada a.
 * 
 * Resumen de Event Delegation
 * Evento: se asocia al padre
 * Target del evento: indica a que hijo delegar el evento
 */
'use strict';
// Immediately-Invoked Function Expression (IIFE)
(function() {
    console.log('RESUELTO CON EVENT DELEGATION');

    var books = document.querySelector('#books');
    /**
     * Aquí se aplica EVENT BUBBLING
        el padre (#books) escucha el evento de los hijos (tag a). 
        Por medio del target accedido desde el evento se puede saber desde que hijo se generó el evento.
     */
    books.addEventListener('click', (evento) => {
        if (evento.target.matches('a#book4')) {
            console.log(` ${evento.target.innerText}. Fue clickeado. No hay stock`);
        } else {
            console.log(`${evento.target.innerText}. Fue clickeado. Hay stock`);
        }
    })
})()