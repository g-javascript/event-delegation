# event-delegation

Descripción: Se expone una lista de libros

Problema: Interactuar con cada uno de los libros

Solución: se muestran dos alternativas posibles
    
    Solución 1) Sin el uso de event delegation
    
    Solución 2) Con el uso de event delegation y event bubbling

## Conceptos a desarrollar
Event Delegation, Event Bubbling

## Tecnologias
Lenguaje: JavaScript

## Uso

### Solución 1

```bash
1) Abrir en el navegador el archivo solucion-1/index.html
```
### Solución 2

```bash
1) Abrir en el navegador el archivo solucion-2/index.html
```

## Referencias
[Dev Education](https://www.youtube.com/watch?v=bOd2HIDh7cU)
