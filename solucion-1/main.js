'use strict';
// Immediately-Invoked Function Expression (IIFE)
(function() {
    console.log('RESUELTO SIN EVENT DELEGATION');
    var book1 = document.querySelector('#book1');
    var book2 = document.querySelector('#book2');
    var book3 = document.querySelector('#book3');
    var book4 = document.querySelector('#book4');
    var book5 = document.querySelector('#book5');

    book1.addEventListener('click', () => {
        console.log(`${book1.innerText}. Fue clickeado. Hay stock`);
    })
    book2.addEventListener('click', () => {
        console.log(`${book2.innerText}. Fue clickeado. Hay stock`);
    })
    book3.addEventListener('click', () => {
        console.log(`${book3.innerText}. Fue clickeado. Hay stock`);
    })
    book4.addEventListener('click', () => {
        console.log(`${book4.innerText}. Fue clickeado. No hay stock`);
    })
    book5.addEventListener('click', () => {
        console.log(`${book5.innerText}. Fue clickeado. Hay stock`);
    })
})()